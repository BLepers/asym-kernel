#include <linux/sched.h>
#include <linux/mm.h>
#include <linux/module.h>
#include <linux/hugetlb.h>
#include <linux/migrate.h>
#include <linux/memcontrol.h>
#include <linux/mm_inline.h>
#include <linux/cpufreq.h>
#include <linux/swap.h>
#include <linux/swapops.h>
#include <linux/mmu_notifier.h>
#include "internal.h"

#include <linux/debugfs.h>

#include <linux/carrefour-hooks.h>
#include <linux/replicate.h>

// EXTERNAL EXPORTS
EXPORT_SYMBOL(sched_setaffinity);
EXPORT_SYMBOL(find_task_by_vpid);
EXPORT_SYMBOL(is_shm);

struct carrefour_options_t carrefour_default_options = {
   .page_bouncing_fix_4k = 0,
   .page_bouncing_fix_2M = 0,
   .async_4k_migrations  = 0,
   .throttle_4k_migrations_limit = 0,
   .throttle_2M_migrations_limit = 0,
};

struct carrefour_options_t carrefour_options;
struct carrefour_hook_stats_t carrefour_hook_stats;

DEFINE_RWLOCK(carrefour_hook_stats_lock);
DEFINE_PER_CPU(struct carrefour_migration_stats_t, carrefour_migration_stats);

static struct dentry *dfs_dir_entry;
static struct dentry *dfs_it_length;

static u64 iteration_length_cycles = 0;

static unsigned disable_4k_migrations_globally = 0;
static unsigned disable_2M_migrations_globally = 0;

int is_huge_addr_sloppy (int pid, unsigned long addr) {
   struct task_struct *task;
   struct mm_struct *mm = NULL;
   struct vm_area_struct *vma;

   pgd_t *pgd;
   pud_t *pud;
   pmd_t *pmd;

   int is_huge = -1;

   rcu_read_lock();
   task = find_task_by_vpid(pid);
   if(task) {
      mm = get_task_mm(task);
   }
   rcu_read_unlock();
   if (!mm)
      goto out;

   down_read(&mm->mmap_sem);

   vma = find_vma(mm, addr);
   if (!vma || addr < vma->vm_start) {
      goto out_locked;
   }
 
   pgd = pgd_offset(mm, addr);
   if (!pgd_present(*pgd )) {
      goto out_locked;
   }

   pud = pud_offset(pgd, addr);
   if(!pud_present(*pud)) {
      goto out_locked;
   }

   pmd = pmd_offset(pud, addr);
   if (!pmd_present(*pmd )) {
      goto out_locked;
   }

   if(pmd_trans_huge(*pmd)) {
      is_huge = 1;
   }
   else if(unlikely(is_vm_hugetlb_page(vma))) {
      is_huge = 1;
   }
   else {
      is_huge = 0;
   }

out_locked:
   up_read(&mm->mmap_sem);
   mmput(mm);

out:
   return is_huge;
}
EXPORT_SYMBOL(is_huge_addr_sloppy);

int page_status_for_carrefour(int pid, unsigned long addr, int * already_treated, int * huge) {
   struct task_struct *task;
   struct mm_struct *mm = NULL;
   struct vm_area_struct *vma;
   struct page * page;
   int ret = -1;
   int err;
   int flags = 0;

   *already_treated = *huge = 0;

   rcu_read_lock();
   task = find_task_by_vpid(pid);
   if(task) {
      mm = get_task_mm(task);
   }
   rcu_read_unlock();
   if (!mm)
      return -1;

   down_read(&mm->mmap_sem);

   vma = find_vma(mm, addr);
   if (!vma || addr < vma->vm_start)
      goto out_locked;

   if (!is_vm_hugetlb_page(vma)) {
      flags |= FOLL_GET;
   }
 
   page = follow_page(vma, addr, flags);

   err = PTR_ERR(page);
   if (IS_ERR(page) || !page)
      goto out_locked;

   ret = 0;

   if(page->stats.nr_migrations) { // Page has been migrated already once
      *already_treated = 1;
   }
   else if(PageReplication(page)) {
      *already_treated = 1;
   }
 
   if (is_vm_hugetlb_page(vma)) {
      if(PageHuge(page)) {
         *huge = 1;
      }
      else {
         DEBUG_PANIC("[WARNING] How could it be possible ?\n");
      }
   }

   if(transparent_hugepage_enabled(vma) && (PageTransHuge(page))) {
      *huge = 2;
   }

   if(flags & FOLL_GET) {
      put_page(page);
   }

out_locked:
   //printk("[Core %d, PID %d] Releasing mm lock (0x%p)\n", smp_processor_id(), pid, &mm->mmap_sem);
   up_read(&mm->mmap_sem);
   mmput(mm);

   return ret;
}
EXPORT_SYMBOL(page_status_for_carrefour);

void reset_carrefour_stats (void) {
   int i = 0;
   write_lock(&carrefour_hook_stats_lock);
   memset(&carrefour_hook_stats, 0, sizeof(struct carrefour_hook_stats_t));
   for(i = 0; i < num_online_cpus(); i++) {
      struct carrefour_migration_stats_t * stats_cpu = per_cpu_ptr(&carrefour_migration_stats, i);
      memset(stats_cpu, 0, sizeof(struct carrefour_migration_stats_t));
   }

   disable_2M_migrations_globally = 0;
   disable_4k_migrations_globally = 0;

   write_unlock(&carrefour_hook_stats_lock);
}
EXPORT_SYMBOL(reset_carrefour_stats);


void reset_carrefour_hooks (void) {
   cpumask_clear(&carrefour_default_options.migr_cpus);
   cpumask_set_cpu(0, &carrefour_default_options.migr_cpus);
   carrefour_options = carrefour_default_options;
   reset_carrefour_stats();
}
EXPORT_SYMBOL(reset_carrefour_hooks);


void configure_carrefour_hooks(struct carrefour_options_t options) {
   carrefour_options = options;
}
EXPORT_SYMBOL(configure_carrefour_hooks);

struct carrefour_options_t get_carrefour_hooks_conf(void) {
   return carrefour_options;
}
EXPORT_SYMBOL(get_carrefour_hooks_conf);


struct carrefour_hook_stats_t get_carrefour_hook_stats(void) {
   struct carrefour_hook_stats_t stats;
   int i;

   write_lock(&carrefour_hook_stats_lock);
   stats = carrefour_hook_stats;
   stats.time_spent_in_migration_4k = 0;
   stats.time_spent_in_migration_2M = 0;
   for(i = 0; i < num_online_cpus(); i++) {
      struct carrefour_migration_stats_t * stats_cpu = per_cpu_ptr(&carrefour_migration_stats, i);
      stats.time_spent_in_migration_4k += stats_cpu->time_spent_in_migration_4k;
      stats.time_spent_in_migration_2M += stats_cpu->time_spent_in_migration_2M;

      stats.nr_4k_migrations += stats_cpu->nr_4k_migrations;
      stats.nr_2M_migrations += stats_cpu->nr_2M_migrations;
   }
   write_unlock(&carrefour_hook_stats_lock);

   return stats;
}
EXPORT_SYMBOL(get_carrefour_hook_stats);

#define STEALING_GRANULARITY 30000 // cpus will process that amount of pages at a time
struct infom {
   int pid;
   int from;
   int to;
   int nb_pages;
   void **addresses;
   int done;
   spinlock_t lock;
};
int _s_migrate_pages_steal(struct infom *info);
int _s_migrate_pages(pid_t pid, unsigned long nr_pages, void ** pages, int * nodes, int throttle, int from);

void s_flush_cpu(void *_i) {
   local_flush_tlb();
}

static DECLARE_WAIT_QUEUE_HEAD(wait_queue_migrate);
static int wait_queue_migrate_flag = 0;
static atomic_t threads_migrating_pages;

void s_migrate_all_pagespar_cpu(void *_i) {
   struct infom *i = _i;
   int me = smp_processor_id(), cpu;
   int nb_cpus = cpumask_weight(&carrefour_options.migr_cpus);
   int nb_pages_per_cpu = i->nb_pages / nb_cpus;
   int nb_cpus_before_me = 0, position_in_mask = -1;
   if(nb_pages_per_cpu < STEALING_GRANULARITY)
      nb_cpus = i->nb_pages / STEALING_GRANULARITY;
   if(!nb_cpus)
      nb_cpus = 1;

   for_each_cpu(cpu, &carrefour_options.migr_cpus) {
      if(cpu == me) {
         position_in_mask = nb_cpus_before_me;
      } else {
         nb_cpus_before_me++;
      }
   }

   // Depending on the number of pages to be migrated, we only allow nb_cpus to migrate pages
   // to reduce useless work / lock contention. Also do not perform any job if we are not in 
   // the list of chosen cpus.
   if(position_in_mask == -1 || position_in_mask >= nb_cpus)
      goto end;

   preempt_disable();
   _s_migrate_pages_steal(i);
   preempt_enable();

end:
   if(atomic_sub_and_test(1, &threads_migrating_pages)) {
      wait_queue_migrate_flag = 1;
      wake_up_interruptible(&wait_queue_migrate);
   }
}

int s_migrate_all_pages(pid_t pid, int from, int to, int throttle) {
   int err = 0;
   struct task_struct *task;
   struct mm_struct *mm = NULL;
   struct vm_area_struct *vma;
   void **addresses;
   size_t nb_pages = 0, nb_max_pages = STEALING_GRANULARITY;
   struct infom info;
   u64 start, stop;
   u64 clock_speed = cpufreq_quick_get(smp_processor_id());
   int nb_recent_mmaps = 0;

   if(!clock_speed)
      clock_speed = cpu_khz;
   clock_speed *= 1000; //kHz -> Hz

   rdtscll(start);

   rcu_read_lock();
   task = find_task_by_vpid(pid);
   if(task) {
      mm = get_task_mm(task);
   }
   rcu_read_unlock();

   if (!mm) {
      printk("Unable to find pid's mm %d\n", pid);
      err = -ESRCH;
      goto out_clean;
   }

	err = migrate_prep();
	if (err)
		return err;




   down_read(&mm->mmap_sem);

   if(1) {
      int i, size = mm->allocation_rdt_index > MAX_RDT_MM ? MAX_RDT_MM : mm->allocation_rdt_index;
      for(i = 0; i < size; i++) {
         if(mm->allocation_rdt[i] > start - carrefour_options.not_migrate_mmap_threshold_time * clock_speed)
            nb_recent_mmaps++;
      }
      if(carrefour_options.not_migrate_mmap_threshold && nb_recent_mmaps > carrefour_options.not_migrate_mmap_threshold) {
         up_read(&mm->mmap_sem);
         mmput(mm);
         goto end_nomigr;
      }
   }

   addresses = vmalloc(nb_max_pages * sizeof(*addresses));
   for (vma = mm->mmap; vma!=NULL; vma = vma->vm_next) {
      unsigned long address;

      for (address = vma->vm_start; address < vma->vm_end; address += PAGE_SIZE) {
         if(nb_pages >= nb_max_pages) {
            void **tmp_addresses;
            nb_max_pages *= 2;
            tmp_addresses = vmalloc(nb_max_pages * sizeof(*addresses));
            memcpy(tmp_addresses, addresses, nb_pages*sizeof(*addresses));
            vfree(addresses);
            addresses = tmp_addresses;
         }
         addresses[nb_pages] = (void*)address;
         nb_pages++;
      }
   }
   up_read(&mm->mmap_sem);
   mmput(mm);

   rdtscll(stop);
   info.pid = pid;
   info.from = from;
   info.to = to;
   info.nb_pages = nb_pages;
   info.addresses = addresses;
   info.lock = __SPIN_LOCK_UNLOCKED(lock);
   info.done = 0;

   //HACK: currently the fast migration algo segfaults on some apps
   //      luckily builtin migr is fast enough on these apps
   if(carrefour_options.async_4k_migrations == 3) {
      if(nb_pages > 500000)
         carrefour_options.async_4k_migrations = 2; //use fast algo
      else if(nb_recent_mmaps == 0)
         carrefour_options.async_4k_migrations = 2; //use fast algo
      else
         carrefour_options.async_4k_migrations = 0; //use builtin
   }

   printk("Planning to migrate %d pages (took %lu cycles) using algo %d on %d cpus\n", (int)nb_pages, (long unsigned)(stop - start), carrefour_options.async_4k_migrations, cpumask_weight(&carrefour_options.migr_cpus));

   preempt_disable();
   if(cpumask_weight(&carrefour_options.migr_cpus) <= 1) {
      _s_migrate_pages_steal(&info);
   } else {
      //on_each_cpu(s_migrate_all_pagespar_cpu, &info, 1);
      atomic_set(&threads_migrating_pages, cpumask_weight(&carrefour_options.migr_cpus));
      wait_queue_migrate_flag = 0;
      smp_call_function_many(&carrefour_options.migr_cpus, s_migrate_all_pagespar_cpu, &info, 0);
      if(cpu_isset(smp_processor_id(), carrefour_options.migr_cpus))
         s_migrate_all_pagespar_cpu(&info); //smp_call does not call the function locally
      wait_event_interruptible(wait_queue_migrate, wait_queue_migrate_flag != 0);
   }
   preempt_enable();

   if(carrefour_options.async_4k_migrations == 2)
      on_each_cpu(s_flush_cpu, &info, 1);
   vfree(addresses);

   rdtscll(stop);
   printk("Done migrating took %lu cycles\n", (long unsigned)(stop - start));

out_clean:
   return err;

end_nomigr:
   printk("Not migrating because number of new vmas (%d) is higher than threshold\n", nb_recent_mmaps);
   return err;
}
EXPORT_SYMBOL(s_migrate_all_pages);

int s_migrate_pages(pid_t pid, unsigned long nr_pages, void ** pages, int * nodes, int throttle) {
   return _s_migrate_pages(pid, nr_pages, pages, nodes, throttle, -1);
}

int _s_migrate_pages_steal(struct infom *info) {
   struct task_struct *task;
   struct mm_struct *mm = NULL;
   int nb_migr_success = 0;

   int i = 0;
   int err = 0;
   int cpu = smp_processor_id();
   u64 start, stop, total_migr = 0, total_alloc = 0, total_copy = 0, total_put = 0, total_rmap = 0;

#define MAX_PUT_BATCH  512
   struct page **pages_to_free = kmalloc(MAX_PUT_BATCH * sizeof(*pages_to_free), GFP_ATOMIC);
   size_t nb_pages_to_free = 0;

   int from = info->from;
   int to = info->to;

   rdtscll(start);

   rcu_read_lock();
   task = find_task_by_vpid(info->pid);
   if(task) {
      mm = get_task_mm(task);
   }
   rcu_read_unlock();

   if (!mm) {
      err = -ESRCH;
      goto out_clean;
   }

   down_read(&mm->mmap_sem);

   while(1) {
      int start_page, end_page;
      spin_lock(&info->lock);
      if(info->done >= info->nb_pages) {
         spin_unlock(&info->lock);
         break;
      } else {
         start_page = info->done;
         end_page = info->done + STEALING_GRANULARITY;
         if(end_page > info->nb_pages)
            end_page = info->nb_pages;
         info->done = end_page;
         spin_unlock(&info->lock);
      }

      for(i = start_page; i < end_page; i++) {
         struct page * page;
         struct vm_area_struct *vma;

         unsigned long addr = (unsigned long) info->addresses[i];
         int current_node;

         pte_t* pte;
         spinlock_t* ptl;

         u64 _start, _stop;

         vma = find_vma(mm, addr);
         if (!vma || addr < vma->vm_start) {
            continue;
         }

         pte = get_locked_pte_from_va (mm->pgd_master, mm, addr, &ptl);
         if(!pte) {
            continue;
         }

         page = pte_page(*pte);
         if (IS_ERR(page) || !page) {
            pte_unmap_unlock(pte, ptl);
            //DEBUG_WARNING("Cannot migrate a NULL page\n");
            continue;
         }

         get_page(page);

         if (!PageAnon(page)) {
            //only move anon pages
            pte_unmap_unlock(pte, ptl);
            put_page(page);
            continue;
         }

         /* Don't want to migrate a replicated page */
         if (PageReplication(page)) {
            pte_unmap_unlock(pte, ptl);
            put_page(page);
            continue;
         }

         if (PageHuge(page) || PageTransHuge(page)) {
            DEBUG_WARNING("[WARNING] What am I doing here ?\n");
            pte_unmap_unlock(pte, ptl);
            put_page(page);
            continue;
         }

         if(carrefour_options.page_bouncing_fix_4k && (page->stats.nr_migrations >= carrefour_options.page_bouncing_fix_4k)) {
            //DEBUG_WARNING("Page bouncing fix enable\n");
            pte_unmap_unlock(pte, ptl);
            put_page(page);
            continue;
         }

         current_node = page_to_nid(page);
         if(current_node == to) {
            //DEBUG_WARNING("Current node (%d) = destination node (%d) for page 0x%lx\n", current_node, nodes[i], addr);
            pte_unmap_unlock(pte, ptl);
            put_page(page);
            continue;
         }

         if(from != -1 && current_node != from) {
            //DEBUG_WARNING("Current node (%d) != from node (%d) for page 0x%lx\n", current_node, nodes[i], addr);
            pte_unmap_unlock(pte, ptl);
            put_page(page);
            continue;
         }

         rdtscll(_start);
         if(carrefour_options.async_4k_migrations == 2) {
            pte_t new_pte;
            struct page *new_page;
            u64 t1,t2;

            pte_unmap_unlock(pte, ptl);

            rdtscll(t1);
            new_page = alloc_page_interleave(GFP_HIGHUSER_MOVABLE, 0, to);
            new_page->index = page->index;
            new_page->mapping = page->mapping;
            new_page->stats = page->stats;
            rdtscll(t2);
            total_alloc += t2 - t1;

            new_pte = mk_pte(new_page, vma->vm_page_prot);

            rdtscll(t1);
            page_remove_rmap(page);
            page_add_new_anon_rmap(new_page, vma, addr);
            rdtscll(t2);
            total_rmap += t2 - t1;

            set_pte_at(mm, addr, pte, new_pte);

            rdtscll(t1);
            copy_highpage(new_page, page);
            rdtscll(t2);
            total_copy += t2 - t1;

#if 0
            atomic_set(&page->_count, 1); //might explode
            put_page(page);  //should free the page
            rdtscll(t1);
            total_put += t1 - t2;
#else
            pages_to_free[nb_pages_to_free++] = page;
            if(nb_pages_to_free == MAX_PUT_BATCH) {
               u64 t1, t2;
               struct zone *zone = page_zone(pages_to_free[0]);
               unsigned long flags;
               int j;

               rdtscll(t1);
               spin_lock_irqsave(&zone->lru_lock, flags);
               for(j = 0; j < nb_pages_to_free; j++) {
                  struct page *page = pages_to_free[j];
                  atomic_set(&page->_count, 0);
                  if (PageLRU(page)) {
                     struct lruvec *lruvec;
                     lruvec = mem_cgroup_page_lruvec(page, zone);
                     __ClearPageLRU(page);
                     del_page_from_lru_list(page, lruvec, page_off_lru(page));
                  }
               }
               spin_unlock_irqrestore(&zone->lru_lock, flags);
               for(j = 0; j < nb_pages_to_free; j++) {
                  struct page *page = pages_to_free[j];
                  free_hot_cold_page(page, 0);
               }
               rdtscll(t2);
               total_put += t2 - t1;

               nb_pages_to_free = 0;
            }
#endif
         } else if(carrefour_options.async_4k_migrations == 1) {
            pte_t new_pte = *pte;
            pte_unmap_unlock(pte, ptl);

            lock_page(page);

            /* Confirm the PTE did not while locked */
            spin_lock(ptl);
            if (likely(pte_same(new_pte, *pte))) {
               // TODO: we should lock the page
               page->dest_node = to;

               // Make sure that pmd is tagged as "NUMA"
               new_pte = pte_mknuma(new_pte);
               set_pte_at(mm, addr, pte, new_pte);

               /** FGAUD: We need to flush the TLB, don't we ? **/
               flush_tlb_page(vma, addr);

               /** And make sure to invalid all copies -- TODO: too many flush **/
               clear_flush_all_node_copies(mm, vma, addr);
            }

            spin_unlock(ptl);

            unlock_page(page);
            put_page(page);
         } else {
            pte_unmap_unlock(pte, ptl);
            migrate_misplaced_page(page, to);
         }
         rdtscll(_stop);
         total_migr += _stop - _start;
         nb_migr_success++;
      }
   }

   if(nb_pages_to_free) {
      u64 t1, t2;
		struct zone *zone = page_zone(pages_to_free[0]);
      unsigned long flags;

      rdtscll(t1);
		spin_lock_irqsave(&zone->lru_lock, flags);
      for(i = 0; i < nb_pages_to_free; i++) {
         struct page *page = pages_to_free[i];
         atomic_set(&page->_count, 0);
         if (PageLRU(page)) {
            struct lruvec *lruvec;
            lruvec = mem_cgroup_page_lruvec(page, zone);
            __ClearPageLRU(page);
            del_page_from_lru_list(page, lruvec, page_off_lru(page));
         }
      }
      spin_unlock_irqrestore(&zone->lru_lock, flags);
      for(i = 0; i < nb_pages_to_free; i++) {
         struct page *page = pages_to_free[i];
         free_hot_cold_page(page, 0);
      }
      rdtscll(t2);
      total_put += t2 - t1;
   }
   kfree(pages_to_free);

   up_read(&mm->mmap_sem);
   mmput(mm);

   rdtscll(stop);
   printk("[CPU %d] _s_migrate_pages %d/%d migrations done took %lu cycles (%lu migration %lu%% %lu alloc %lu%% %lu copy %lu%% %lu put %lu%% %lu rmap %lu%%)\n", cpu, (int)nb_migr_success, info->nb_pages, (long unsigned)(stop-start),
         (long unsigned)total_migr,
         (long unsigned)(total_migr*100L/(stop-start)),
         (long unsigned)total_alloc,
         (long unsigned)(total_alloc*100L/(stop-start)),
         (long unsigned)total_copy,
         (long unsigned)(total_copy*100L/(stop-start)),
         (long unsigned)total_put,
         (long unsigned)(total_put*100L/(stop-start)),
         (long unsigned)total_rmap,
         (long unsigned)(total_rmap*100L/(stop-start))
      );
out_clean:
   return err;
}


int _s_migrate_pages(pid_t pid, unsigned long nr_pages, void ** pages, int * nodes, int throttle, int from) {
   struct task_struct *task;
   struct mm_struct *mm = NULL;
   int nb_migr_success = 0;

   int i = 0;
   int err = 0;
   int cpu = smp_processor_id();
   u64 start, stop, total_migr = 0, total_alloc = 0, total_copy = 0, total_put = 0;

   struct page **pages_to_free = kmalloc(MAX_PUT_BATCH * sizeof(*pages_to_free), GFP_ATOMIC);
   size_t nb_pages_to_free = 0;

   rdtscll(start);

   rcu_read_lock();
   task = find_task_by_vpid(pid);
   if(task) {
      mm = get_task_mm(task);
   }
   rcu_read_unlock();

   if (!mm) {
      err = -ESRCH;
      goto out_clean;
   }

   down_read(&mm->mmap_sem);

   for(i = 0; i < nr_pages; i++) {
      struct page * page;
      struct vm_area_struct *vma;

      unsigned long addr = (unsigned long) pages[i];
      int current_node;

      pte_t* pte;
      spinlock_t* ptl;

      u64 _start, _stop;

      vma = find_vma(mm, addr);
      if (!vma || addr < vma->vm_start) {
         continue;
      }

      pte = get_locked_pte_from_va (mm->pgd_master, mm, addr, &ptl);
      if(!pte) {
         continue;
      }

      page = pte_page(*pte);

      if (IS_ERR(page) || !page) {
         pte_unmap_unlock(pte, ptl);
         //DEBUG_WARNING("Cannot migrate a NULL page\n");
         continue;
      }

      get_page(page);

      if (!PageAnon(page)) {
         //only move anon pages
         pte_unmap_unlock(pte, ptl);
         put_page(page);
         continue;
      }

      /* Don't want to migrate a replicated page */
      if (PageReplication(page)) {
         pte_unmap_unlock(pte, ptl);
         put_page(page);
         continue;
      }

      if (PageHuge(page) || PageTransHuge(page)) {
         DEBUG_WARNING("[WARNING] What am I doing here ?\n");
         pte_unmap_unlock(pte, ptl);
         put_page(page);
         continue;
      }

      if(carrefour_options.page_bouncing_fix_4k && (page->stats.nr_migrations >= carrefour_options.page_bouncing_fix_4k)) {
         //DEBUG_WARNING("Page bouncing fix enable\n");
         pte_unmap_unlock(pte, ptl);
         put_page(page);
         continue;
      }

      current_node = page_to_nid(page);
      if(current_node == nodes[i]) {
         //DEBUG_WARNING("Current node (%d) = destination node (%d) for page 0x%lx\n", current_node, nodes[i], addr);
         pte_unmap_unlock(pte, ptl);
         put_page(page);
         continue;
      }

      if(from != -1 && current_node != from) {
         //DEBUG_WARNING("Current node (%d) != from node (%d) for page 0x%lx\n", current_node, nodes[i], addr);
         pte_unmap_unlock(pte, ptl);
         put_page(page);
         continue;
      }

      rdtscll(_start);
      if(throttle || carrefour_options.async_4k_migrations == 0) {
         unsigned allowed = migration_allowed_4k();
         pte_unmap_unlock(pte, ptl);

         if(allowed && migrate_misplaced_page(page, nodes[i])) {
            //__DEBUG("Migrating page 0x%lx\n", addr);

            // FGAUD
            if(migration_callback) {
               migration_callback(mm, addr); 
            }
         }
         else {
            if(!allowed)
               __DEBUG("migration was not allowed\n");
            //DEBUG_WARNING("[WARNING] Migration of page 0x%lx failed !\n", addr);
         }
      } else if(carrefour_options.async_4k_migrations == 2) {
         pte_t new_pte;
         struct page *new_page;
         u64 t1,t2;

         pte_unmap_unlock(pte, ptl);

         rdtscll(t1);
         new_page = alloc_page_interleave(GFP_HIGHUSER_MOVABLE, 0, nodes[i]);
         rdtscll(t2);
         total_alloc += t2 - t1;

         new_pte = mk_pte(new_page, vma->vm_page_prot);

         page_remove_rmap(page); 
         page_add_new_anon_rmap(new_page, vma, addr);

         set_pte_at(mm, addr, pte, new_pte); 
         //flush_tlb_page(vma, addr); //tlb will be flushed after

         rdtscll(t1);
         copy_highpage(new_page, page);
         rdtscll(t2);
         total_copy += t2 - t1;

         //atomic_set(&page->_count, 1); //might explode
         //put_page(page); // should free the page
         //rdtscll(t1);
         //total_put += t1 - t2;
         
         pages_to_free[nb_pages_to_free++] = page;
         if(nb_pages_to_free == MAX_PUT_BATCH) {
            u64 t1, t2;
            struct zone *zone = page_zone(pages_to_free[0]);
            unsigned long flags;
            int j;

            rdtscll(t1);
            spin_lock_irqsave(&zone->lru_lock, flags);
            for(j = 0; j < nb_pages_to_free; j++) {
               struct page *page = pages_to_free[j];
               atomic_set(&page->_count, 0);
               if (PageLRU(page)) {
                  struct lruvec *lruvec;
                  lruvec = mem_cgroup_page_lruvec(page, zone);
                  __ClearPageLRU(page);
                  del_page_from_lru_list(page, lruvec, page_off_lru(page));
               }
            }
            spin_unlock_irqrestore(&zone->lru_lock, flags);
            for(j = 0; j < nb_pages_to_free; j++) {
               struct page *page = pages_to_free[j];
               free_hot_cold_page(page, 0);
            }
            rdtscll(t2);
            total_put += t2 - t1;

            nb_pages_to_free = 0;
         }
      }
      else {
         pte_t new_pte = *pte;
         pte_unmap_unlock(pte, ptl);

         lock_page(page);

         /* Confirm the PTE did not while locked */
         spin_lock(ptl);
         if (likely(pte_same(new_pte, *pte))) {
            // TODO: we should lock the page
            page->dest_node = nodes[i];

            // Make sure that pmd is tagged as "NUMA"
            new_pte = pte_mknuma(new_pte);
            set_pte_at(mm, addr, pte, new_pte);

            /** FGAUD: We need to flush the TLB, don't we ? **/
            flush_tlb_page(vma, addr);

            /** And make sure to invalid all copies -- TODO: too many flush **/
            clear_flush_all_node_copies(mm, vma, addr);
         }

         spin_unlock(ptl);

         unlock_page(page);
         put_page(page);
      }
      rdtscll(_stop);
      total_migr += _stop - _start;
      nb_migr_success++;
   }

   if(nb_pages_to_free) {
      u64 t1, t2;
		struct zone *zone = page_zone(pages_to_free[0]);
      unsigned long flags;

      rdtscll(t1);
		spin_lock_irqsave(&zone->lru_lock, flags);
      for(i = 0; i < nb_pages_to_free; i++) {
         struct page *page = pages_to_free[i];
         atomic_set(&page->_count, 0);
         if (PageLRU(page)) {
            struct lruvec *lruvec;
            lruvec = mem_cgroup_page_lruvec(page, zone);
            __ClearPageLRU(page);
            del_page_from_lru_list(page, lruvec, page_off_lru(page));
         }
      }
      spin_unlock_irqrestore(&zone->lru_lock, flags);
      for(i = 0; i < nb_pages_to_free; i++) {
         struct page *page = pages_to_free[i];
         free_hot_cold_page(page, 0);
      }
      rdtscll(t2);
      total_put += t2 - t1;
   }
   kfree(pages_to_free);

   up_read(&mm->mmap_sem);
   mmput(mm);

   rdtscll(stop);
   printk("[CPU %d] _s_migrate_pages %d/%d migrations done took %lu cycles (%lu migration %lu%% %lu alloc %lu%% %lu copu %lu%% %lu pu %lu%%)\n", cpu, (int)nb_migr_success, (int)nr_pages, (long unsigned)(stop-start),
         (long unsigned)total_migr,
         (long unsigned)(total_migr*100L/(stop-start)),
         (long unsigned)total_alloc,
         (long unsigned)(total_alloc*100L/(stop-start)),
         (long unsigned)total_copy,
         (long unsigned)(total_copy*100L/(stop-start)),
         (long unsigned)total_put,
         (long unsigned)(total_put*100L/(stop-start))
      );
out_clean:
   return err;
}
EXPORT_SYMBOL(s_migrate_pages);

static struct page *new_page(struct page *p, unsigned long private, int **x)
{
   // private containe the destination node
   return alloc_huge_page_node(page_hstate(p), private);
}

int s_migrate_hugepages(pid_t pid, unsigned long nr_pages, void ** pages, int * nodes) {
   struct task_struct *task;
   struct mm_struct *mm = NULL;

   int i = 0;
	
#if ENABLE_MIGRATION_STATS
   uint64_t start_migr, end_migr, migrated = 0;
   rdtscll(start_migr);
#endif

   rcu_read_lock();
   task = find_task_by_vpid(pid);
   if(task) {
      mm = get_task_mm(task);
   }
   rcu_read_unlock();

   if (!mm)
      return -ESRCH;

	down_read(&mm->mmap_sem);

   for(i = 0; i < nr_pages; i++) {
      struct page * hpage;

      // Get the current page
      struct vm_area_struct *vma;
      int ret;

      unsigned long addr = (unsigned long) pages[i];
      int current_node;

      vma = find_vma(mm, addr);
      //if (!vma || pp->addr < vma->vm_start || !vma_migratable(vma))
      if (!vma || addr < vma->vm_start || !is_vm_hugetlb_page(vma))
         continue;

      hpage = follow_page(vma, addr, 0);

      if (IS_ERR(hpage) || !hpage)
         continue;

      if(hpage != compound_head(hpage)) {
         DEBUG_WARNING("[WARNING] What's going on ?\n");
         continue;
      }

      if(! PageHuge(hpage)) {
         DEBUG_WARNING("[WARNING] What am I doing here ?\n");
         continue;
      }

      current_node = page_to_nid(hpage);
      if(current_node != nodes[i]) {
         // Migrate the page
         if(get_page_unless_zero(hpage)) {
            ret = migrate_huge_page(hpage, new_page, nodes[i], MIGRATE_SYNC);
            put_page(hpage);

            if(ret) {
               printk("[WARNING] Migration of page 0x%lx failed !\n", addr);
            }
            else {
#if ENABLE_MIGRATION_STATS
               INCR_REP_STAT_VALUE(migr_2M_from_to_node[current_node][nodes[i]], 1);
               migrated = 1;
#endif
            }
         }
      }
   }

   up_read(&mm->mmap_sem);
   mmput(mm);

#if ENABLE_MIGRATION_STATS
   rdtscll(end_migr);
   INCR_MIGR_STAT_VALUE(2M, (end_migr - start_migr), migrated);
#endif
   return 0;
}
EXPORT_SYMBOL(s_migrate_hugepages);

// Quick and dirty for now. TODO: update
int move_thread_to_node(pid_t tid, int node) {
   int ret;
   ret = sched_setaffinity(tid, cpumask_of_node(node));
   sched_setaffinity(tid, cpu_online_mask);

   return ret;
}
EXPORT_SYMBOL(move_thread_to_node);

struct task_struct * get_task_struct_from_pid(int pid) {
   struct task_struct * task;

   rcu_read_lock();
   task = find_task_by_vpid(pid);
   if(task) {
      get_task_struct(task);
   }
   rcu_read_unlock();

   return task;
}
EXPORT_SYMBOL(get_task_struct_from_pid);

int find_and_split_thp(int pid, unsigned long addr) {
   struct task_struct *task;
   struct mm_struct *mm = NULL;
   struct vm_area_struct *vma;
   struct page * page;
   int ret = 1;
   int err;

   u64 start, stop;
   rdtscll(start);

   rcu_read_lock();
   task = find_task_by_vpid(pid);
   if(task) {
      mm = get_task_mm(task);
   }
   rcu_read_unlock();
   if (!mm) {
      ret = -ESRCH;
      return ret;
   }

   down_read(&mm->mmap_sem);

   vma = find_vma(mm, addr);
   if (!vma || addr < vma->vm_start) {
      ret = -EPAGENOTFOUND;
      goto out_locked;
   }

   /*if (!transparent_hugepage_enabled(vma)) {
      ret = -EINVALIDPAGE;
      goto out_locked;
   }*/
 
   page = follow_page(vma, addr, FOLL_GET);

   err = PTR_ERR(page);
   if (IS_ERR(page) || !page) {
      ret = -EPAGENOTFOUND;
      goto out_locked;
   }

   if(PageTransHuge(page)) {
      // We found the page. Split it.
      // split_huge_page does not create new pages. It will simple create new ptes and update the pmd
      // see  __split_huge_page_map for details
      ret = split_huge_page(page);
   }
   else {
      ret = -EINVALIDPAGE;
   }

   put_page(page);

out_locked:
   //printk("[Core %d, PID %d] Releasing mm lock (0x%p)\n", smp_processor_id(), pid, &mm->mmap_sem);
   up_read(&mm->mmap_sem);
   mmput(mm);

   rdtscll(stop);
   carrefour_hook_stats.split_nb_calls++;
   carrefour_hook_stats.time_spent_in_split += (stop - start);

   return ret;
}
EXPORT_SYMBOL(find_and_split_thp);

int find_and_migrate_thp(int pid, unsigned long addr, int to_node) {
   struct task_struct *task;
   struct mm_struct *mm = NULL;
   struct vm_area_struct *vma;
   struct page *page;
   int ret = -EAGAIN;
   int current_node;

   pgd_t *pgd;
   pud_t *pud;
   pmd_t *pmd, orig_pmd;

   rcu_read_lock();
   task = find_task_by_vpid(pid);
   if(task) {
      mm = get_task_mm(task);
   }
   rcu_read_unlock();
   if (!mm) {
      ret = -ESRCH;
      return ret;
   }

   down_read(&mm->mmap_sem);

   vma = find_vma(mm, addr);
   if (!vma || addr < vma->vm_start) {
      ret = -EPAGENOTFOUND;
      goto out_locked;
   }

   /*if (!transparent_hugepage_enabled(vma)) {
      ret = -EINVALIDPAGE;
      goto out_locked;
   }*/
 
   pgd = pgd_offset(mm, addr);
   if (!pgd_present(*pgd )) {
      ret = -EINVALIDPAGE;
      goto out_locked;
   }

   pud = pud_offset(pgd, addr);
   if(!pud_present(*pud)) {
      ret = -EINVALIDPAGE;
      goto out_locked;
   }

   pmd = pmd_offset(pud, addr);
   if (!pmd_present(*pmd ) || !pmd_trans_huge(*pmd)) {
      ret = -EINVALIDPAGE;
      goto out_locked;
   }

   // We found a valid pmd for this address and that's a THP
   orig_pmd = *pmd;

   // Mostly copied from the do_huge_pmd_numa_page function
	spin_lock(&mm->page_table_lock);
	if (unlikely(!pmd_same(orig_pmd, *pmd))) {
      spin_unlock(&mm->page_table_lock);
		goto out_locked;
   }

	page = pmd_page(*pmd);
	get_page(page);

	current_node = page_to_nid(page);

   if(current_node == to_node) {
		put_page(page);
      spin_unlock(&mm->page_table_lock);
      ret = -ENOTMISPLACED;
		goto out_locked;
   }
	spin_unlock(&mm->page_table_lock);

	/* Acquire the page lock to serialise THP migrations */
	lock_page(page);

	/* Confirm the PTE did not while locked */
	spin_lock(&mm->page_table_lock);
	if (unlikely(!pmd_same(orig_pmd, *pmd))) {
		unlock_page(page);
		put_page(page);
      spin_unlock(&mm->page_table_lock);
		goto out_locked;
	}

   if(carrefour_options.page_bouncing_fix_2M && (page->stats.nr_migrations >= carrefour_options.page_bouncing_fix_2M)) {
		unlock_page(page);
		put_page(page);
      spin_unlock(&mm->page_table_lock);
      ret = -EBOUNCINGFIX;
		goto out_locked;
   }


   page->dest_node = to_node;

   // Make sure that pmd is tagged as "NUMA"
   orig_pmd = pmd_mknuma(orig_pmd);
   set_pmd_at(mm, addr & PMD_MASK, pmd, orig_pmd);
   
   /** FGAUD: We need to flush the TLB, don't we ? **/
   flush_tlb_page(vma, addr);

	spin_unlock(&mm->page_table_lock);

   if(carrefour_options.sync_thp_migration) {
      /* Migrate the THP to the requested node */
      ret = migrate_misplaced_transhuge_page(mm, vma, pmd, orig_pmd, addr, page, to_node);

      if (ret > 0) {
         //__DEBUG("Migrated THP 0x%lx successfully\n", addr);
         ret = 0;
      }
      else {
         //__DEBUG("Failed migrating THP 0x%lx (ret = %d)\n", addr, ret);
         ret = -1;
         // put page has been performed by migrate_misplaced_transhuge_page

         // It failed
         // We need to clear the pmd_numa_flag ...   
         spin_lock(&mm->page_table_lock);
         if (pmd_same(orig_pmd, *pmd)) {
            orig_pmd = pmd_mknonnuma(orig_pmd);
            set_pmd_at(mm, addr, pmd, orig_pmd);
            VM_BUG_ON(pmd_numa(*pmd));
            update_mmu_cache_pmd(vma, addr, pmd);
         }
         spin_unlock(&mm->page_table_lock);
      }
   }
   else {
      ret = 0;
      unlock_page(page);
      put_page(page);
   }

out_locked:
   //printk("[Core %d, PID %d] Releasing mm lock (0x%p)\n", smp_processor_id(), pid, &mm->mmap_sem);
   up_read(&mm->mmap_sem);
   mmput(mm);

   return ret;
}
EXPORT_SYMBOL(find_and_migrate_thp);

enum thp_states get_thp_state(void) {
#ifdef CONFIG_TRANSPARENT_HUGEPAGE
   if(test_bit(TRANSPARENT_HUGEPAGE_FLAG, &transparent_hugepage_flags)) {
      return THP_ALWAYS;
   }
   else if(test_bit(TRANSPARENT_HUGEPAGE_REQ_MADV_FLAG, &transparent_hugepage_flags)) {
      return THP_MADVISE;
   }
   else {
      return THP_DISABLED;
   }
#else
   return THP_DISABLED;
#endif
}
EXPORT_SYMBOL(get_thp_state);

void set_thp_state(enum thp_states state) {
#ifdef CONFIG_TRANSPARENT_HUGEPAGE
   if(state == THP_DISABLED) {
      clear_bit(TRANSPARENT_HUGEPAGE_FLAG, &transparent_hugepage_flags);
      clear_bit(TRANSPARENT_HUGEPAGE_REQ_MADV_FLAG, &transparent_hugepage_flags);
   }
   else if(state == THP_ALWAYS) {
      set_bit(TRANSPARENT_HUGEPAGE_FLAG, &transparent_hugepage_flags);
      clear_bit(TRANSPARENT_HUGEPAGE_REQ_MADV_FLAG, &transparent_hugepage_flags);
   }
   else if(state == THP_MADVISE){
      clear_bit(TRANSPARENT_HUGEPAGE_FLAG, &transparent_hugepage_flags);
      set_bit(TRANSPARENT_HUGEPAGE_REQ_MADV_FLAG, &transparent_hugepage_flags);
   }
   else {
      DEBUG_WARNING("Unknown state!\n");
   }
#else
   DEBUG_WARNING("Cannot change THP state since THP is not enabled in kernel options...\n");
#endif
}
EXPORT_SYMBOL(set_thp_state);


unsigned migration_allowed_2M(void) {
   unsigned t;
   unsigned allowed;
   struct carrefour_migration_stats_t* stats;
   
   if(disable_2M_migrations_globally)
      return 0;

   if(!carrefour_options.throttle_2M_migrations_limit)
      return 1;

   read_lock(&carrefour_hook_stats_lock);
   stats = get_cpu_ptr(&carrefour_migration_stats);

   t = iteration_length_cycles ? (stats->time_spent_in_migration_2M * 100UL) / (iteration_length_cycles) : 0;
   
   allowed = t < carrefour_options.throttle_2M_migrations_limit;
   if(!allowed) {
      __DEBUG("Thresold %u %% reached. Disabling 2M migrations\n", carrefour_options.throttle_2M_migrations_limit);
      disable_2M_migrations_globally = 1;
   }

   put_cpu_ptr(&carrefour_migration_stats);
   read_unlock(&carrefour_hook_stats_lock);

/*__DEBUG("THROTTLE: %llu %llu %u\n", 
         (long long unsigned) carrefour_hook_stats.time_spent_in_migration_2M, 
         (long long unsigned) iteration_length_cycles * num_online_cpus(), 
         t);*/

   return allowed;
}

unsigned migration_allowed_4k(void) {
   unsigned t;
   unsigned allowed;
   struct carrefour_migration_stats_t* stats;

   if(disable_4k_migrations_globally)
      return 0;

   if(!carrefour_options.throttle_4k_migrations_limit)
      return 1;

   read_lock(&carrefour_hook_stats_lock);
   stats = get_cpu_ptr(&carrefour_migration_stats);

   t = iteration_length_cycles ? (stats->time_spent_in_migration_4k * 100UL) / (iteration_length_cycles) : 0;

   allowed = t < carrefour_options.throttle_4k_migrations_limit;
   if(!allowed) {
      __DEBUG("Thresold %u %% reached. Disabling 4k migrations\n", carrefour_options.throttle_4k_migrations_limit);
      disable_4k_migrations_globally = 1;
   }

   put_cpu_ptr(&carrefour_migration_stats);
   read_unlock(&carrefour_hook_stats_lock);

   /*__DEBUG("THROTTLE: %llu %llu %u\n", 
         (long long unsigned) carrefour_hook_stats.time_spent_in_migration_4k[smp_processor_id()], 
         (long long unsigned) iteration_length_cycles * num_online_cpus(), 
         t);*/

   return allowed;
}

static int __init carrefour_hooks_init(void) {
   dfs_dir_entry = debugfs_create_dir("carrefour", NULL);
   dfs_it_length = debugfs_create_u64("iteration_length_cycles", 0666, dfs_dir_entry, &iteration_length_cycles);

   rwlock_init(&carrefour_hook_stats_lock);

   printk("Initializing Carrefour hooks\n");
   reset_carrefour_hooks();

   return 0;
}
module_init(carrefour_hooks_init)
